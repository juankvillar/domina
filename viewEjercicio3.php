<?php
$loc = 3;
include_once('header.php');
if (isset($_POST['hora'])) {
    include('models/Ejercicio3.php');
    $obj = new Reloj();
    $grados = $obj->calcularGrados($_POST['hora']);
}
?>
<div class="container">
    <div class="alert alert-info" role="alert">
        <p>Crear una función que toma como parámetro un string de hora y minutos “hh:mm”, y luego
            devuelve el ángulo menor entre la mano de la hora y la mano del minuto. El formato de la
            hora y minutos debe ser con dos dígitos, “01:45”, “10:30”, “02:25”, “00:00”, “12:30”,
            “12:05”, “12:12”, “12:27”. Y se puede asumir que la mano de la hora siempre estará justo
            en una hora sin importar cuantos minutos han pasado. También, si el parámetro de la
            función no fue puesto correctamente o si la hora y minuto no es numérico, la función debe
            tirar un error.</p>
    </div>
    <form method="post">
        <div class="form-group">
            <label for="hora">Digitar hora:</label>
            <input value="<?php echo isset($_POST['hora']) ? $_POST['hora'] : ''; ?>" type="text" class="form-control" name="hora" placeholder="Digitar hora en formato de 12 horas 00:00">
            <?php if (isset($_POST['hora'])) { ?>
                <?php if ($grados['val']) { ?>
                    <p class="text-success"><?php echo 'El angulo más agudo entre ambas manecillas del reloj es: ' ?></p>
                    <h3 class="text-success"> <?php echo $grados['msj'] ?> &#176;</h3>
                <?php } else { ?>
                    <p class="text-danger"><?php echo $grados['msj'] ?></p>
                <?php } ?>
            <?php } ?>
        </div>
        <button type="submit" class="btn btn-success" name="calcular">Calcular</button>
    </form>
</div>
<?php
include_once('footer.php');
?>