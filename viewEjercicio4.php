<?php
$loc = 4;
include_once('header.php');
if (isset($_POST['hora'])) {
    include('models/Ejercicio3.php');
    $obj = new Reloj();
    $grados = $obj->calcularGrados($_POST['hora']);
}
?>
<div class="container">
    <div class="alert alert-info" role="alert">
        <p>Un par de números naturales (i, j) está representado por el número correspondiente a la
            celda en la fila i y columna j. Por ejemplo, el par (3, 2) está representado por el número
            natural 17.
            Crear una función que toma como parámetros (tamaño fila, tamaño columna, posición fila,
            posición columna), debe retornar el número correspondiente de la posición de la fila y la
            columna. Si la posición fila o columna es mayor al tamaño del arreglo debe arrojar un error.</p>
    </div>
</div>
<?php 
    matriz(3,3,1,2);


    function matriz( $filas, $columnas, $fila, $columna ) {
        
        if( $fila > $filas) {
            return "ERROR: Fila fuera de rango";
            die();
        }

        if( $columnas < $columna ) {
            return "ERROR: columna fuera de rango";
            die();
        }
        
        $contador = 0;
        $matriz = array();
        
        for($fil = 0;$fil <= $filas; $fil++ ) {
            
            for($col = 0; $col <= $columnas; $col++) {
                $matriz[$fil][$col] = $contador;
                $contador++;
                echo $contador.' ';
            }
            echo '<br>';
        }        
        echo "<h3 class='text-success'> la posición de la matríz, con la fila $fila y la columna $columna es: {$matriz[$fila][$columna]}</h3>";
    }

?>
<?php
include_once('footer.php');
?>