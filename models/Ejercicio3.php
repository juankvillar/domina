<?php
class Reloj {

    public $horario;
    public $minutero;
    public $gradosMaxi;
    public $gradosMit;

    public function __construct() {

        $this->horario = 30; // Se divide 360 grados entre las 12 horas totales para obtener el total de grados que corre la aguja por hora
        $this->minutero = 6; // Se divide 360 grados entre los 60 munutostotales para obtener el total de grados que corre la aguja por minuto
        $this->gradosMit = 180; // Se tiene para optener el angulo agudo y no el optuso
        $this->gradosMaxi = 360; // Se tiene para optener el angulo agudo y no el optuso

    }


    public function validarHora( $hora ) {
        
        $hora = trim( $hora ); // borramos los espacios al inicio y al  final
        $res['val'] = true; // variable para validación de formato de hora
        
        if( strlen( $hora ) != 5 ){
            $res['val'] = false;
            $res['msj'] = 'no tiene 5 caracteres';
        }elseif( !is_numeric( $hora[0] ) || !is_numeric( $hora[1] ) || !is_numeric( $hora[3] ) || !is_numeric( $hora[4] ) ){
            $res['val'] = false;
            $res['msj'] = 'Alguno de los valos no es númerico';
        }elseif( $hora[0] != 0 && $hora[0] != 1  ){
            $res['val'] = false;
            $res['msj'] = 'El primer valor de la hora está mal';
        }elseif( $hora[0].$hora[1] > 12 ){
            $res['val'] = false;
            $res['msj'] = 'La hora supera el limite permitido, máximo 12';
        }elseif( $hora[3].$hora[4] > 59 ){
            $res['val'] = false;
            $res['msj'] = 'Los minutos superas el limite permitido, máximo 59';
        }elseif( $hora[2] != ':' ){
            $res['val'] = false;
            $res['msj'] = 'El signo divisor debe ser : estás usando '. $hora[2];
        }

        if( $res['val'] ){
            $res['hora'] = $hora[0] != 0 ? 1 : '' ;
            $res['hora'] .= $hora[1] == 0 ? 0  : $hora[1] ;
            $res['hora'] = $res['hora'] == '' ? 0 : $res['hora'] ;

            $res['minutos'] = $hora[3] != 0 ? $hora[3] : '' ;
            $res['minutos'] .= $hora[4] == 0 ? 0 : $hora[4] ;
            $res['minutos'] = $res['minutos'] == '' ? 0 : $res['minutos'] ;

            $res['val'] = true;
        }else{
            $res['val'] = false;
        }
        
        return $res;
    }

    public function calcularGrados( $hora ) {
        $res = $this->validarHora( $hora );
        if ( $res['val'] ) {
            $res = $this->validarHora( $hora );
            $graHor = $res['hora'] * $this->horario; // Se calcula los grados por hora
            $graMin = $res['minutos'] * $this->minutero; // e calcula los grados por minutos
            $totGra = abs($graHor - $graMin); // Se calculan los grados entre ambas agujas del relos
            // echo 'Grados hora'.$graHor.'<br>';
            // echo 'Grados minutos'.$graMin.'<br>';
            // echo 'Grados total'.$totGra.'<br>';
            $grados['val'] = true;
            if( $totGra > $this->gradosMit ){ // Si el grado total es mayor de 180, se le resta 360 para obtener el lado más angosto de los angulos
                $grados['msj'] = $this->gradosMaxi - $totGra;
            }else{
                $grados['msj'] = abs( $graHor - $graMin); // Si el grado total no es mayor de 180, se deja el mismo valor
            }
            
        }else{
            $grados['val'] = false;
            $grados['msj'] = $res['msj'];
        }
        return $grados;
    }
    
} // end classs
?>
