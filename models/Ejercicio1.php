<?php
class Ejercicio1 {

    public $contador;

    function __construct() {
        $this->contador = 0;
    }

    public function contarArray($arr) {
        $arr = trim($arr);
        $arr = trim($arr, ' , ');

        if ($arr == '') {
            $this->contador = 'Enviaste un valor vacío';
        } else {
            $arr = str_replace(' ', '', $arr); // quitamos todos los espacios
            $arr = explode(',', $arr); // convertimos el string en un array


            foreach ($arr as $value) {

                if ($value == 8) {
                    $this->contador = $this->contador + 5;
                    continue; // saltamos al siguiente array
                }

                if (($value % 2) == 0) {
                    $this->contador = $this->contador + 1;
                } else {
                    $this->contador = $this->contador + 3;
                }
            } // end foreach
        } // end if
    } // end function

} // end class
