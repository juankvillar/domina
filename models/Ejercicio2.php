<?php
class Ejercicio2 {

    public $matriz = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

    function __construct() {
        
    }

    public function sumarArray( $val1, $val2 ) {
        array_search( $val1, $this->matriz );
        $res = 0;
        if ( $val1 < 0 || $val2 < 0) { // Se valida que ambos numeros son positivos
            $res = -1;
        }elseif ( $val1 > $val2 ) { // Se valida que el primer valor sea menor que el mayor
            $res = 0;
        }elseif ( array_search( $val1, $this->matriz ) === false && array_search( $val2, $this->matriz ) === false ) {
            $res = 0;
        }elseif ( is_numeric( array_search( $val1, $this->matriz ) )) { // Se valida que el primer numero este en la matríz
            $posIni = array_search( $val1, $this->matriz ); // Se captura la posición inicial
            $posFin = array_search( $val2, $this->matriz ); // Si el segundo número no está en la matríz y cómo ya se valido que no es negativo, se sobre entiende que mayor
            $posFin = !$posFin ? count(  $this->matriz ) - 1 : $posFin; // Se le asigna el valor máximo del array
            
            for ($i=$posIni; $i <= $posFin ; $i++) { 
                $res += $this->matriz[$i];
            } // end for

        }
        
        return $res;
    } // end function

} // end class
