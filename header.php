<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <title>Pruebas Domina</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="icon" type="image/png" sizes="32x32" href="https://www.domina.com.co/favicon/favicon.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <ul class="nav navbar-nav">
      <li <?php if($loc ==  1){ echo 'class="active"'; }?> ><a href="viewEjercicio1.php">Ejercicio 1</a></li>
      <li <?php if($loc ==  2){ echo 'class="active"'; }?> ><a href="viewEjercicio2.php">Ejercicio 2</a></li>
      <li <?php if($loc ==  3){ echo 'class="active"'; }?> ><a href="viewEjercicio3.php">Ejercicio 3</a></li>
      <li <?php if($loc ==  4){ echo 'class="active"'; }?> ><a href="viewEjercicio4.php">Ejercicio 4</a></li>
      <li <?php if($loc ==  5){ echo 'class="active"'; }?> ><a href="viewEjercicio5.php">Ejercicio 5</a></li>
    </ul>
  </div>
</nav>