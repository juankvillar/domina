<?php
$loc = 1;
include_once('header.php');
if (isset($_POST['calcular'])) {
    include('models/Ejercicio1.php');
    $obj = new Ejercicio1();
    $obj->contarArray($_POST['numeros']);
}
?>
<div class="container">
    <div class="alert alert-info" role="alert">
        <p>Dada un arreglo de números enteros, calcule una puntuación total según las siguientes reglas:</p>
        <ul>
            <li>Agregue 1 punto por cada número par en el arreglo.</li>
            <li>Suma 3 puntos por cada número impar en el arreglo.</li>
            <li>Agregue 5 puntos por cada vez que encuentre un 8 en el arreglo.</li>
        </ul>
        <br>
        <p>Ejemplos</p>
        <ol>
            <li>[1,2,3,4,5], respuesta = 11</li>
            <li>[15,25,35], respuesta = 9</li>
            <li>[8,8], respuesta = 10</li>
        </ol>
    </div>
    <form method="post">
        <div class="form-group">
            <label for="numeros">Por favor separe los numeros por una coma ( , ) para crear el array:</label>
            <input value="<?php echo isset($_POST['numeros']) ? $_POST['numeros'] : ''; ?>" type="text" class="form-control" name="numeros" placeholder="Digite los números separados por coma" required>
            <?php if (isset($_POST['numeros'])) { ?>
                <p class="text-success"><?php echo 'La puntuación total es: ' ?></p>
                <h3 class="text-success"> <?php echo $obj->contador ?></h3>
            <?php } ?>
        </div>
        <button type="submit" class="btn btn-success" name="calcular">Calcular</button>
    </form>
</div>
<?php
include_once('footer.php');
?>