<?php
$loc = 2;
include_once('header.php');
if (isset($_POST['calcular'])) {
    include('models/Ejercicio2.php');
    $obj = new Ejercicio2();
    $sumatoria = $obj->sumarArray( $_POST['val1'], $_POST['val2'] );
}
?>
<div class="container">
    <div class="alert alert-info" role="alert">
        <p>2. En este ejercicio, estamos trabajando con un arreglo de 10 enteros, como sigue: [10, 20, 30,
            40, 50, 60, 70, 80, 90, 100]. 0 es el primer índice y 9 es el último índice de la matriz.
            Escribe una función que reciba dos números enteros como parámetros. La función devuelve
            la suma de los elementos de la matriz que se encuentran entre esos dos números enteros.</p>
        <p>Por ejemplo, si usamos 30 y 60 como parámetros, la función debería devolver 180.
            Algunos requisitos adicionales:</p>
        <ul>
            <li>Los dos enteros pasados a la función deben ser positivos; si no, la función deberíadevolver -1.</li>
            <li>Valide que el primer número entero sea menor que el segundo número entero. De lo contrario, la función debería devolver 0.</li>
            <li>Si el primer número entero está en la matriz y el segundo está por encima de 100, por ejemplo, 90 y 120, entonces la función debe devolver la suma de los números enteros que están dentro del arreglo y entre el rango dado. En este caso, eso sería 190.</li>
            <li>Si no se encuentran ambos enteros en la matriz, por ejemplo 110 y 120, la función debería devolver 0.</li>
        </ul>

    </div>
    <form method="post">
        <div class="form-group">
            <label for="val1">Digitar primer valor:</label>
            <input step="10" value="<?php echo isset($_POST['val1']) ? $_POST['val1'] : ''; ?>" type="number" class="form-control" name="val1" required>
        </div>
        <div class="form-group">
            <label for="val2">Digitar segundo valor:</label>
            <input step="10" value="<?php echo isset($_POST['val2']) ? $_POST['val2'] : ''; ?>" type="number" class="form-control" name="val2" required>
        </div>
        <p> <?php if ( isset( $sumatoria ) ) { ?>
            <p class="text-success"> La puntuación total es: </p>
            <h3 class="text-success"> <?php echo $sumatoria; ?></h3>
         </p>
         <?php } ?>
        <button type="submit" class="btn btn-success" name="calcular">Calcular</button>
    </form>
</div>
<?php
include_once('footer.php');
?>